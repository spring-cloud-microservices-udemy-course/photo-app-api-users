FROM eclipse-temurin:21 
VOLUME /tmp 
COPY target/PhotoAppApiUsers-0.0.1-SNAPSHOT.jar PhotoAppApiUsers.jar 
ENTRYPOINT ["java","-jar","PhotoAppApiUsers.jar"]