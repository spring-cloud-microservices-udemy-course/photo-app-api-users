package com.danmamaliga.photoapp.api.users.ui.controllers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danmamaliga.photoapp.api.users.service.UsersService;
import com.danmamaliga.photoapp.api.users.shared.UserDto;
import com.danmamaliga.photoapp.api.users.ui.model.CreateUserRequestModel;
import com.danmamaliga.photoapp.api.users.ui.model.CreateUserResponseModel;
import com.danmamaliga.photoapp.api.users.ui.model.UserResponseModel;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/users")
public class UsersController {

	private final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	private Environment environment;

	@Autowired
	private UsersService usersService;

	public UsersController() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@GetMapping("/status/check")
	public String status() {
		return "Working on port %s, with token secret: %s".formatted(environment.getProperty("local.server.port"),
				environment.getProperty("token.secret"));
	}

	@PostMapping
	public ResponseEntity<CreateUserResponseModel> createUser(@Valid @RequestBody CreateUserRequestModel userDetails) {
		UserDto userDto = modelMapper.map(userDetails, UserDto.class);
		UserDto createdUser = usersService.createUser(userDto);
		CreateUserResponseModel response = modelMapper.map(createdUser, CreateUserResponseModel.class);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	// @PreAuthorize("hasRole('ADMIN') or principal == #userId")
	// @PreAuthorize("principal == #userId")
	// @PostAuthorize("principal == returnObject.getBody().userId")
	@GetMapping(value = "/{userId}")
	public ResponseEntity<UserResponseModel> getUser(@PathVariable String userId,
			@RequestHeader("Authorization") String authorization) {
		UserDto userDto = usersService.getUserByUserId(userId, authorization);
		UserResponseModel userResponseModel = modelMapper.map(userDto, UserResponseModel.class);
		return ResponseEntity.ok(userResponseModel);
	}

	@PreAuthorize("hasRole('ADMIN') or hasAuthority('DELETE') or principal == #userId")
	@DeleteMapping("/{userId}")
	public String deleteUser(@PathVariable("userId") String userId) {
		usersService.deleteUser(userId);
		return "Deleting user with id %s".formatted(userId);
	}
}
