package com.danmamaliga.photoapp.api.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.danmamaliga.photoapp.api.users.data.AuthorityEntity;
import com.danmamaliga.photoapp.api.users.data.AuthorityRepository;
import com.danmamaliga.photoapp.api.users.data.RoleEntity;
import com.danmamaliga.photoapp.api.users.data.RoleRepository;
import com.danmamaliga.photoapp.api.users.data.UserEntity;
import com.danmamaliga.photoapp.api.users.data.UsersRepository;
import com.danmamaliga.photoapp.api.users.shared.Roles;

import jakarta.transaction.Transactional;

@Component
public class InitialUsersSetup {

	@Autowired
	AuthorityRepository authorityRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	UsersRepository usersRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Transactional
	@EventListener
	public void onApplicationEvent(ApplicationReadyEvent event) {
		log.info("InitialUsersSetup started...");
		AuthorityEntity readAuthority = creatAuthority("READ");
		AuthorityEntity writeAuthority = creatAuthority("WRITE");
		AuthorityEntity deleteAuthority = creatAuthority("DELETE");

		createRole(Roles.ROLE_USER.name(), Arrays.asList(readAuthority, writeAuthority));
		RoleEntity roleAdmin = createRole(Roles.ROLE_ADMIN.name(),
				Arrays.asList(readAuthority, writeAuthority, deleteAuthority));

		if (roleAdmin != null) {
			UserEntity admin = new UserEntity();
			admin.setFirstName("Dan");
			admin.setLastName("Mamaliga");
			admin.setEmail("admin@test.com");
			admin.setUserId(UUID.randomUUID().toString());
			admin.setEncryptedPassword(bCryptPasswordEncoder.encode("1234"));
			admin.setRoles(Arrays.asList(roleAdmin));

			UserEntity storedAdminUser = usersRepository.findByEmail("admin@test.com");

			if (storedAdminUser == null) {
				usersRepository.save(admin);
			}
		}
		log.info("InitialUsersSetup end.");

	}

	@Transactional
	private AuthorityEntity creatAuthority(String name) {
		AuthorityEntity authority = authorityRepository.findByName(name);
		if (authority == null) {
			authority = new AuthorityEntity(name);
			authorityRepository.save(authority);
		}
		return authority;
	}

	@Transactional
	private RoleEntity createRole(String name, Collection<AuthorityEntity> authorities) {
		RoleEntity role = roleRepository.findByName(name);
		if (role == null) {
			role = new RoleEntity(name, authorities);
			roleRepository.save(role);
		}
		return role;
	}

}
