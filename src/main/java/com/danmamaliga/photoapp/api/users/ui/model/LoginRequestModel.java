package com.danmamaliga.photoapp.api.users.ui.model;

public record LoginRequestModel(String email, String password) {

}
