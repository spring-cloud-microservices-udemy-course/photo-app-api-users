package com.danmamaliga.photoapp.api.users.commons.web;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

public class WebClientFactory {

	public static WebClient.Builder create() {
		HttpClient httpClient = createHttpClient();
		WebClient.Builder clientBuilder = WebClient.builder()
				.clientConnector(new ReactorClientHttpConnector(httpClient));
		return clientBuilder;
	}

	private static HttpClient createHttpClient() {
		HttpClient httpClient = HttpClient.create().option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
				.responseTimeout(Duration.ofMillis(5000))
				.doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
						.addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));
		return httpClient;
	}

}
