package com.danmamaliga.photoapp.api.users.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.danmamaliga.photoapp.api.users.data.AlbumsServiceClient;
import com.danmamaliga.photoapp.api.users.data.AuthorityEntity;
import com.danmamaliga.photoapp.api.users.data.RoleEntity;
import com.danmamaliga.photoapp.api.users.data.UserEntity;
import com.danmamaliga.photoapp.api.users.data.UsersRepository;
import com.danmamaliga.photoapp.api.users.shared.UserDto;
import com.danmamaliga.photoapp.api.users.ui.model.AlbumResponseModel;

import feign.FeignException;
import jakarta.transaction.Transactional;

@Service
public class UserServiceImpl implements UsersService {

	private final ModelMapper modelMapper = new ModelMapper();
	Logger log = LoggerFactory.getLogger(getClass());

	UsersRepository usersRepository;
	BCryptPasswordEncoder bCryptPasswordEncoder;
	WebClient webClient;
	Environment environment;
	AlbumsServiceClient albumsServiceClient;

	public UserServiceImpl(UsersRepository usersRepository, BCryptPasswordEncoder bCryptPasswordEncoder,
			WebClient.Builder webClientBuilder, Environment environment, AlbumsServiceClient albumsServiceClient) {
		this.usersRepository = usersRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.webClient = webClientBuilder.build();
		this.environment = environment;
		this.albumsServiceClient = albumsServiceClient;
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@Override
	public UserDto createUser(UserDto userDetails) {
		userDetails.setUserId(UUID.randomUUID().toString());
		userDetails.setEncryptedPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));
		UserEntity userEntity = modelMapper.map(userDetails, UserEntity.class);
		usersRepository.save(userEntity);
		UserDto userDto = modelMapper.map(userEntity, UserDto.class);
		return userDto;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = usersRepository.findByEmail(username);
		if (userEntity == null) {
			throw new UsernameNotFoundException(username);
		}
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		Collection<RoleEntity> roles = userEntity.getRoles();
		roles.forEach(role -> {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
			Collection<AuthorityEntity> roleAuthorities = role.getAuthorities();
			roleAuthorities
					.forEach(roleAuthority -> authorities.add(new SimpleGrantedAuthority(roleAuthority.getName())));
		});
		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), true, true, true, true, authorities);
	}

	@Override
	public UserDto getUserDetailsByEmail(String email) {
		UserEntity userEntity = usersRepository.findByEmail(email);
		if (userEntity == null) {
			throw new UsernameNotFoundException(email);
		}
		return modelMapper.map(userEntity, UserDto.class);
	}

	@Override
	public UserDto getUserByUserId(String userId, String authorization) {
		UserEntity user = usersRepository.findByUserId(userId);
		if (user == null) {
			throw new RuntimeException("User with userId: %s not found!".formatted(userId));
		}
		UserDto userDto = modelMapper.map(user, UserDto.class);
		log.debug("Before calling albums Microservice");
		List<AlbumResponseModel> albums = albumsServiceClient.getAlbums(userId, authorization);
//		List<AlbumResponseModel> albums = getAlbumsUsingWebClient(userId);
		log.debug("After calling albums Microservice");
		userDto.setAlbums(albums);
		return userDto;
	}

	List<AlbumResponseModel> getAlbumsUsingWebClient(String userId) {
		String albumUrl = environment.getProperty("albums.url").formatted(userId);
		ResponseEntity<List<AlbumResponseModel>> response = webClient.get().uri(URI.create(albumUrl)).retrieve()
				.toEntity(new ParameterizedTypeReference<List<AlbumResponseModel>>() {
				}).block();
		List<AlbumResponseModel> albums = List.of();
		if (response != null) {
			albums = response.getBody();
		}
		return albums;
	}

	@Transactional
	@Override
	public void deleteUser(String userId) {
		usersRepository.deleteByUserId(userId);
	}

}
