package com.danmamaliga.photoapp.api.users.ui.model;

public record AlbumResponseModel(String albumId, String userId, String name, String description) {

}
