package com.danmamaliga.photoapp.api.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.reactive.function.client.WebClient;

import com.danmamaliga.photoapp.api.users.commons.web.WebClientFactory;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class PhotoAppApiUsersApplication {

	@Autowired
	Environment environment;

	Logger log = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) {
		SpringApplication.run(PhotoAppApiUsersApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public HttpExchangeRepository httpTraceRepository() {
		return new InMemoryHttpExchangeRepository();
	}

	@LoadBalanced
	@Bean
	public WebClient.Builder loadBalancedWebClientBuilder() {
		return WebClientFactory.create();
	}

	@Profile("production")
	@Bean
	feign.Logger.Level feignLoggerLevel() {
		return feign.Logger.Level.NONE;
	}

	@Profile("!production")
	@Bean
	feign.Logger.Level defaultFeignLoggerLevel() {
		return feign.Logger.Level.FULL;
	}

	@Bean
	@Profile("production")
	public String createProductionBean() {
		log.info("Production bean created. myapplication.environment = %s"
				.formatted(environment.getProperty("myapplication.environment")));
		return "Production bean";
	}

	@Bean
	@Profile("!production")
	public String createNotProductionBean() {
		log.info("Not production bean created. myapplication.environment = %s"
				.formatted(environment.getProperty("myapplication.environment")));
		return "Not production bean";
	}

	@Bean
	@Profile("default")
	public String createDevelopmentBean() {
		log.info("Development bean created. myapplication.environment = %s"
				.formatted(environment.getProperty("myapplication.environment")));
		return "Development bean";
	}

}
