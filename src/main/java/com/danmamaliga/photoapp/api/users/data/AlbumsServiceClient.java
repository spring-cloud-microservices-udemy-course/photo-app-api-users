package com.danmamaliga.photoapp.api.users.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.danmamaliga.photoapp.api.users.ui.model.AlbumResponseModel;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;

@FeignClient("albums-ws")
public interface AlbumsServiceClient {

	@Retry(name = "albums-ws")
	@CircuitBreaker(name = "albums-ws", fallbackMethod = "getAlbumsFallback")
	@GetMapping("/users/{userId}/albums")
	public List<AlbumResponseModel> getAlbums(@PathVariable String userId,
			@RequestHeader("Authorization") String authorization);

	default List<AlbumResponseModel> getAlbumsFallback(String userId, String authorization, Throwable exception) {
		System.out.println("User id: %s".formatted(userId));
		System.out.println("Exception took place: %s".formatted(exception.getMessage()));
		return new ArrayList<>();
	}

}
